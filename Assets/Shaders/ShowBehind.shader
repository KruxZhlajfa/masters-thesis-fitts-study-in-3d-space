﻿Shader "Custom/Show Behing Objects" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_OutlineColor ("Outline Color", Color) = (1,1,0,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
 
	SubShader {
		Tags { "Queue"="Geometry+5" }
		LOD 200

		//Occluded pass
		Pass {
			Tags { "LightMode" = "Always" }
			Cull Off
			ZWrite Off
			ZTest Greater
			ColorMask RGB // alpha not used
 
			Blend SrcAlpha OneMinusSrcAlpha 
			Color[_OutlineColor]
		}

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
 
	Fallback "Diffuse", 1
}
