﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SetUIElementValues : MonoBehaviour {

	private SessionController s;

	void Start () {
		s = SessionController.session;

		if (gameObject.name == "Start Button" && (s.userID.Equals("") || s.userID.Equals("testuser") || s.pointingID.Equals(null) || s.shapeID.Equals(null) || s.AW.Equals(null)))
			gameObject.GetComponent<Button> ().interactable = false;
		else if (gameObject.name == "UserID Field" && !(s.userID.Equals("")) && !(s.userID.Equals("testuser")))
			gameObject.GetComponent<InputField> ().text = s.userID;
		else if (gameObject.name == "Pointing Device Dropdown" && !(s.pointingID.Equals(null)))
			gameObject.GetComponent<Dropdown> ().value = s.pointingID;
		else if (gameObject.name == "Shape Dropdown" && !(s.shapeID.Equals(null)))
			gameObject.GetComponent<Dropdown> ().value = s.shapeID;
		else if (gameObject.name == "A-W Dropdown" && !(s.AW.Equals(null)))
			gameObject.GetComponent<Dropdown> ().value = s.AW;
	}

	void Update () {
		if (gameObject.name == "Start Button" && !(s.userID.Equals("")) && !(s.userID.Equals("testuser")) && !(s.pointingID.Equals(null)) && !(s.shapeID.Equals(null)))
			gameObject.GetComponent<Button> ().interactable = true;
	}
}
