﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class PopupPanel : MonoBehaviour {

	public Text question;
	public Button positiveBtn;
	public Button negativeBtn;
	public GameObject popupPanelObject;
	public GameObject errorPanel;
	public Text error;
	public GameObject UIControlPanel;
	public Text firstKey;
	public Text secondKey;

	private static PopupPanel popupPanel;

	public static PopupPanel Instance () {
		if (!popupPanel) {
			popupPanel = FindObjectOfType (typeof(PopupPanel)) as PopupPanel;
			if (!popupPanel)
				Debug.LogError ("There needs to be at least one active PopupPanel script on a GameObject in your scene.");
		}

		return popupPanel;
	}

	//popup the menu
	public void popup (string question, string error, bool displayButtons, string positiveBtnText, string negativeBtnText, 
							bool displayUIControl, string firstKeyText, string secondKeyText, UnityAction positiveEvent, UnityAction negativeEvent) {
		popupPanelObject.SetActive (true);

		this.question.text = question;

		if (displayButtons) {
			positiveBtn.onClick.RemoveAllListeners ();
			positiveBtn.onClick.AddListener (positiveEvent);
			positiveBtn.onClick.AddListener (ClosePanel);

			negativeBtn.onClick.RemoveAllListeners ();
			negativeBtn.onClick.AddListener (negativeEvent);
			negativeBtn.onClick.AddListener (ClosePanel);

			this.positiveBtn.gameObject.SetActive (true);
			this.negativeBtn.gameObject.SetActive (true);

			this.positiveBtn.GetComponentInChildren<Text> ().text = positiveBtnText;
			this.negativeBtn.GetComponentInChildren<Text> ().text = negativeBtnText;
		} else if (!displayButtons) {
			positiveBtn.onClick.RemoveAllListeners ();
			negativeBtn.onClick.RemoveAllListeners ();

			this.positiveBtn.gameObject.SetActive (false);
			this.negativeBtn.gameObject.SetActive (false);
		}

		if (error != null) {
			errorPanel.SetActive (true);
			this.error.text = error;
		} else {
			errorPanel.SetActive (false);
		}

		if (displayUIControl) {
			UIControlPanel.SetActive (true);
			firstKey.text = firstKeyText;
			secondKey.text = secondKeyText;
		} else {
			UIControlPanel.SetActive (false);
		}

	}

	public void ClosePanel () {
		popupPanelObject.SetActive (false);
	}
}
