﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TargetInstantiation : MonoBehaviour { //script for instantiating targets

	public GameObject prefabTarget;
	public GameObject prefabCircum;
	public Transform player;
	public int[] targetOrder = null; //order in which targets will be highlighted
	public List <GameObject> targets = null;
	public GameObject leapHandCtrl;

	private float radiusOfCircumsphere = 0;
	private Vector3 centerOfCircumsphere = Vector3.zero;
	private float diameterOfTargets = 0;
	private GameController gameController;
	private GameObject circumsphere = null;
	private GameObject targetClone;
	private Vector3[] targetCoordinates = null;

	// Use this for initialization
	void Start () {
		gameController = GameController.Instance ();

		targets = new List <GameObject> ();

		switch (SessionController.session.AW) {
		case GameController.AW_ID_EASY:
			leapHandCtrl.transform.position = new Vector3 (10, 6, -8);
			gameObject.transform.position = new Vector3 (11.17f, 11.38f, -23.34f); //change camera position
			diameterOfTargets = 2.5f;
			radiusOfCircumsphere = 6;
			centerOfCircumsphere = new Vector3 (0, radiusOfCircumsphere+diameterOfTargets, 0);
			player.position = centerOfCircumsphere;
			break;
		case GameController.AW_ID_NORMAL:
			leapHandCtrl.transform.position = new Vector3 (8, 6, -8);
			gameObject.transform.position = new Vector3 (8.13f, 10.53f, -19.26f); //change camera position
			diameterOfTargets = 1.8f;
			radiusOfCircumsphere = 5;
			centerOfCircumsphere = new Vector3 (0, radiusOfCircumsphere+3, 0);
			player.position = centerOfCircumsphere;
			break;
		case GameController.AW_ID_HARD:
			leapHandCtrl.transform.position = new Vector3 (6, 6, -5);
			gameObject.transform.position = new Vector3 (7.02f, 10.35f, -16.21f); //change camera position
			diameterOfTargets = 1;
			radiusOfCircumsphere = 4;
			centerOfCircumsphere = new Vector3 (0, radiusOfCircumsphere+4, 0);
			player.position = centerOfCircumsphere;
			break;
		default:
			Debug.LogError ("No difficulty was chosen in the start menu!");
			break;
		}

		switch (SessionController.session.shapeID) {
		case GameController.SHAPE_ID_TETRAHEDRON:
			targetCoordinates = GetTetrahedronCoordinates (radiusOfCircumsphere, centerOfCircumsphere);
			break;
		case GameController.SHAPE_ID_OCTAHEDRON: 
			targetCoordinates = GetOctahedronCoordinates (radiusOfCircumsphere, centerOfCircumsphere);
			break;
		case GameController.SHAPE_ID_CUBE:
			targetCoordinates = GetCubeCoordinates (radiusOfCircumsphere, centerOfCircumsphere);
			break;
		case GameController.SHAPE_ID_COMPOUND:
			targetCoordinates = GetCompoundCoordinates (radiusOfCircumsphere, centerOfCircumsphere);
			break;
		default:
			Debug.LogError ("No shape was chosen in the start menu!");
			break;
		}
		InstantiateTargets (targetCoordinates, diameterOfTargets, radiusOfCircumsphere*2, centerOfCircumsphere);
		gameController.HighlightNextTarget (null, false, System.DateTime.MinValue);
	}

	private void InstantiateTargets(Vector3[] coordinates, float diameterOfTargets, float diameterOfCircumsphere, Vector3 center){
		for (int i = 0; i < coordinates.Length; i++) {
			targetClone = Instantiate (prefabTarget, coordinates[i], Quaternion.identity) as GameObject;
			targetClone.transform.localScale = new Vector3 (diameterOfTargets, diameterOfTargets, diameterOfTargets);
			targetClone.name = "Target - " + (targets.Count + 1);
			targets.Add (targetClone);
		}
		circumsphere = Instantiate (prefabCircum, center, Quaternion.identity) as GameObject;
		circumsphere.transform.localScale = new Vector3 (diameterOfCircumsphere, diameterOfCircumsphere, diameterOfCircumsphere);
		circumsphere.name = "Circumscribed Sphere";
	}

	private Vector3[] GetTetrahedronCoordinates(float R, Vector3 center){
		float a;
		Vector3[] coordinates = new Vector3[4];

		a = (4 * R) / (Mathf.Sqrt (6));

		coordinates [0] = center + new Vector3 (a/2, 0, -(a/(2*Mathf.Sqrt(2))));
		coordinates [1] = center + new Vector3 (-a/2, 0, -(a/(2*Mathf.Sqrt(2))));
		coordinates [2] = center + new Vector3 (0, a/2, a/(2*Mathf.Sqrt(2)));
		coordinates [3] = center + new Vector3 (0, -a/2, a/(2*Mathf.Sqrt(2)));

		targetOrder = new int[5] {2, 1, 0, 3, 2};

		return coordinates;
	}

	private Vector3[] GetOctahedronCoordinates(float R, Vector3 center){
		float a;
		Vector3[] coordinates = new Vector3[6];

		a = 2 * R / Mathf.Sqrt (2);

		coordinates [0] = center + new Vector3 ((a*Mathf.Sqrt(2))/2, 0, 0);
		coordinates [1] = center + new Vector3 (-(a*Mathf.Sqrt(2))/2, 0, 0);
		coordinates [2] = center + new Vector3 (0, (a*Mathf.Sqrt(2))/2, 0);
		coordinates [3] = center + new Vector3 (0, -(a*Mathf.Sqrt(2))/2, 0);
		coordinates [4] = center + new Vector3 (0, 0, (a*Mathf.Sqrt(2))/2);
		coordinates [5] = center + new Vector3 (0, 0, -(a*Mathf.Sqrt(2))/2);

		targetOrder = new int[7] {2, 1, 4, 3, 5, 0, 2};

		return coordinates;
	}

	private Vector3[] GetCubeCoordinates(float R, Vector3 center){
		float a;
		Vector3[] coordinates = new Vector3[8];

		a = 2 * R / Mathf.Sqrt (3);

		//top layer
		coordinates [0] = center + new Vector3 (-a/2,a/2,-a/2); //lower-left
		coordinates [1] = center + new Vector3 (a/2,a/2,-a/2); //lower-right
		coordinates [2] = center + new Vector3 (a/2,a/2,a/2); //upper-right
		coordinates [3] = center + new Vector3 (-a/2,a/2,a/2); //upper-left

		//bottom layer
		coordinates [4] = center + new Vector3 (-a/2,-a/2,-a/2); //lower-left
		coordinates [5] = center + new Vector3 (a/2,-a/2,-a/2); //lower-right
		coordinates [6] = center + new Vector3 (a/2,-a/2,a/2); //upper-right
		coordinates [7] = center + new Vector3 (-a/2,-a/2,a/2); //upper-left

		targetOrder = new int[9] {0, 3, 7, 6, 2, 1, 5, 4, 0};

		return coordinates;
	}

	private Vector3[] GetCompoundCoordinates(float R, Vector3 center){
		float a;
		Vector3[] coordinates = new Vector3[14];

		a = 2 * R / Mathf.Sqrt (3);

		coordinates [0] = center + new Vector3 (0, R, 0); //up
		coordinates [1] = center + new Vector3 (0, -R, 0); //down
		coordinates [2] = center + new Vector3 (R, 0, 0); //right
		coordinates [3] = center + new Vector3 (-R, 0, 0); //left
		coordinates [4] = center + new Vector3 (0, 0, R); //backward
		coordinates [5] = center + new Vector3 (0, 0, -R); //forward

		//top layer
		coordinates [6] = center + new Vector3 (-a/2,a/2,-a/2); //lower-left
		coordinates [7] = center + new Vector3 (a/2,a/2,-a/2); //lower-right
		coordinates [8] = center + new Vector3 (a/2,a/2,a/2); //upper-right
		coordinates [9] = center + new Vector3 (-a/2,a/2,a/2); //upper-left

		//bottom layer
		coordinates [10] = center + new Vector3 (-a/2,-a/2,-a/2); //lower-left
		coordinates [11] = center + new Vector3 (a/2,-a/2,-a/2); //lower-right
		coordinates [12] = center + new Vector3 (a/2,-a/2,a/2); //upper-right
		coordinates [13] = center + new Vector3 (-a/2,-a/2,a/2); //upper-left

		targetOrder = new int[17] {0, 10, 4, 7, 3, 12, 5, 9, 1, 6, 2, 13, 5, 8, 3, 11, 0};

		return coordinates;
	}
}
