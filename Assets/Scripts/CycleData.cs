﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CycleData {
	
	private List<SubtaskData> subtasks;

	public CycleData(){
		subtasks = new List<SubtaskData> ();
	}

	private float Mean(List<float> values){
		float sum = 0;
		for (int i = 0; i < values.Count; i++)
		{
			sum += values[i];
		}

		return sum / values.Count;
	}

	private float Variance(List<float> values)
	{
		float variance = 0;
		float mean = this.Mean (values);

		for (int i = 0; i < values.Count; i++)
		{
			variance += Mathf.Pow((values[i] - mean), 2);
		}

		return variance / (values.Count-1); //variance based on a sample (not population)
	}

	private float StandardDeviation(List<float> values)
	{
		float variance = this.Variance(values);

		return Mathf.Sqrt(variance);
	}

	public List<SubtaskData> Subtasks{
		get { return this.subtasks; }
	}

	public float A{
		get {
			return Vector3.Distance (subtasks [0].From, subtasks [0].To);
		}
	}

	public float Idn{
		get {
			return Mathf.Log (((this.A/subtasks[0].TargetDiameter)+1), 2);
		}
	}

	public float Mt{
		get { 
			List<float> durations = new List<float> ();
			foreach (SubtaskData subtask in subtasks) {
				durations.Add (subtask.Duration);
			}
			return this.Mean (durations);
		}
	}

	public float AeMean{
		get { 
			List<float> aes = new List<float> (); //holds all Ae values from each subtask
			foreach (SubtaskData subtask in subtasks) {
				aes.Add (subtask.Ae);
			}
			return this.Mean (aes);
		}
	}

	public float We{
		get {
			List<float> dxs = new List<float> (); //holds all Ae values from each subtask
			foreach (SubtaskData subtask in subtasks) {
				dxs.Add (subtask.Dx);
			}
			float SDx = StandardDeviation (dxs);
			float pi = Mathf.PI;
			float e = Mathf.Exp (1);

			return Mathf.Sqrt (2 * pi * e) * SDx;
		}
	}

	public float Ide{
		get {
			return Mathf.Log ((this.AeMean/this.We)+1, 2);
		}
	}

	public float Throughput{
		get {
			return this.Ide / (this.Mt/1000);
		}
	}

}