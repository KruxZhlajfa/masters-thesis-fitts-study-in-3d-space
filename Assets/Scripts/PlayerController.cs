﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Leap.Unity;
using Leap;

public class PlayerController : MonoBehaviour {

	public float speed;
	public SceneController sceneController;
	public Material matInside;
	public Material matOutside;

	private GameController gameController;
	private Controller leapCtrl;
	private SessionController s;
	private CharacterController player;
	private Vector3 handPalmPosition;
	private bool playerInBounds; //boolean to see if it's a hit or miss
	private bool isVGControllerConnected;
	private bool isRightPointingConnected;
	private string[] joyNames;
	private int connectedPointingID;
	private AudioSource audioSource;
	private Vector3 offset;
	private Frame frame;
	private Hand hand;

	void Start() {
		player = GetComponent<CharacterController> ();
		gameController = GameController.Instance ();
		s = SessionController.session;
		audioSource = GetComponent<AudioSource> ();
		leapCtrl = new Controller (); //Leap Motion controller
		isVGControllerConnected = false;
		connectedPointingID = GameController.POINTING_ID_MOUSE; //mouse is the default pointing device
		isRightPointingConnected = false;
		offset = new Vector3(0, 4.5f, 0);
	}

	void Update() { //called before rendering a frame
		if (!gameController.isCycleCompleted) {
			CheckPointingDevice ();

			if (isRightPointingConnected) {
				Cursor.visible = false;
				sceneController.ClosePopup ();

				if ((Input.GetButtonDown ("Submit") && connectedPointingID == GameController.POINTING_ID_GAME)
				    	|| (Input.GetMouseButtonDown (0) && connectedPointingID == GameController.POINTING_ID_MOUSE))
					SubmitHit ();

				float moveHorizontal = 0.0f;
				float moveVertical = 0.0f;
				float moveUpDown = 0.0f;
				Vector3 movementHorizontal;
				Vector3 movementVertical;
				Vector3 movementUpDown;
				if (connectedPointingID == GameController.POINTING_ID_LEAP) {
					if (leapCtrl.IsConnected) {
						Debug.Log ("Getting frame");
						frame = leapCtrl.Frame (); //The latest frame
						hand = null;
						if (frame.Hands.Count > 0) {
							hand = frame.Hands [0];
						}
					} else {
						frame = null;
						hand = null;
					}

					handPalmPosition = hand.PalmPosition.ToVector3();
					handPalmPosition = handPalmPosition * 0.05f - offset;
					handPalmPosition.z *= -1;

					Vector3 newPosition = Camera.main.transform.TransformDirection (handPalmPosition);
					gameObject.transform.position = newPosition;
				} else if (connectedPointingID == GameController.POINTING_ID_GAME) {
					moveHorizontal = Input.GetAxis ("Controller X");
					moveVertical = Input.GetAxis ("Controller Y");
					moveUpDown = Input.GetAxis ("Controller Z");
				} else if (connectedPointingID == GameController.POINTING_ID_MOUSE) {
					moveHorizontal = Input.GetAxis ("Movement X");
					moveVertical = Input.GetAxis ("Movement Y");
					moveUpDown = Input.GetAxis ("Movement Z");
				}

				movementHorizontal = new Vector3 (moveHorizontal, 0, 0);
				movementHorizontal = Camera.main.transform.TransformDirection (movementHorizontal);

				movementVertical = new Vector3 (0, 0, moveVertical);
				movementVertical = Camera.main.transform.TransformDirection (movementVertical);
				movementVertical.y = 0.0f;

				bool positiveNum = moveUpDown > 0;

				if (positiveNum && player.transform.position.y >= 31) //insurance that player won't get out of walls
					movementUpDown = new Vector3 (0, 0, 0);
				else
					movementUpDown = new Vector3 (0, moveUpDown, 0);

				Vector3 movement = movementHorizontal + movementUpDown + movementVertical;

				player.Move (movement * speed);
			} else { //wrong pointing device is connected
				Cursor.visible = true;
				if (s.pointingID == GameController.POINTING_ID_MOUSE)
					sceneController.ErrorPanel ("Please only connect the mouse to continue!");
				else if (s.pointingID == GameController.POINTING_ID_GAME)
					sceneController.ErrorPanel ("Please only connect the video game controller to continue!");
				else if (s.pointingID == GameController.POINTING_ID_LEAP)
					sceneController.ErrorPanel ("Please only connect the Leap Motion controller to continue!");
				else
					sceneController.ErrorPanel ("Please connect pointing device to continue!");
			}
		} else {
			if ((Input.GetButtonDown ("Restart") && connectedPointingID == GameController.POINTING_ID_GAME)
				|| (Input.GetKeyDown (KeyCode.Y) && (connectedPointingID == GameController.POINTING_ID_MOUSE || connectedPointingID == GameController.POINTING_ID_LEAP)))
				sceneController.LoadScene ("StartMenu");
			else if ((Input.GetButtonDown ("Exit") && connectedPointingID == GameController.POINTING_ID_GAME)
				|| (Input.GetKeyDown (KeyCode.E) && (connectedPointingID == GameController.POINTING_ID_MOUSE || connectedPointingID == GameController.POINTING_ID_LEAP)))
				Application.Quit ();
		}
	}

	public void SubmitHit(){
		if (!gameController.isCycleCompleted && connectedPointingID == s.pointingID) {
			if (!playerInBounds)
				audioSource.Play ();
			gameController.HighlightNextTarget (this.gameObject, playerInBounds, System.DateTime.Now);
		}
	}

	void CheckPointingDevice(){ //checks which pointing device is connected and if it's the correct one from start menu
		joyNames = Input.GetJoystickNames ();
		isVGControllerConnected = (joyNames.Length > 0 && joyNames [0].Equals ("Wireless Controller")) ? true : false;

		if (leapCtrl.IsConnected && s.pointingID == GameController.POINTING_ID_LEAP) {
			isRightPointingConnected = true;
			connectedPointingID = GameController.POINTING_ID_LEAP;
		} else if (isVGControllerConnected && s.pointingID == GameController.POINTING_ID_GAME && !leapCtrl.IsConnected) {
			isRightPointingConnected = true;
			connectedPointingID = GameController.POINTING_ID_GAME;
		} else if (Input.mousePresent && s.pointingID == GameController.POINTING_ID_MOUSE && !leapCtrl.IsConnected && !isVGControllerConnected) {
			isRightPointingConnected = true;
			connectedPointingID = GameController.POINTING_ID_MOUSE;
		} else {
			isRightPointingConnected = false;
			connectedPointingID = GameController.POINTING_ID_NONE;
		}
	}

	void OnTriggerStay(Collider other) {
		gameObject.GetComponent<Renderer> ().sharedMaterial = matInside;
		float targetRadius = gameController.GetSphereDiameter (other.gameObject)/2;
		if (Vector3.Distance (player.transform.position, other.transform.position) < targetRadius && other.gameObject.CompareTag ("CurrentTarget")) {
			playerInBounds = true;
		} else {
			playerInBounds = false;
		}
	}

	void OnTriggerExit(Collider other) {
		gameObject.GetComponent<Renderer> ().sharedMaterial = matOutside;
		playerInBounds = false;
	}
}