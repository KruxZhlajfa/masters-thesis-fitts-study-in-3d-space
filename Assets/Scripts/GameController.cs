﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System;

public class GameController : MonoBehaviour {

	public const int POINTING_ID_MOUSE = 0;
	public const int POINTING_ID_GAME = 1;
	public const int POINTING_ID_LEAP = 2;
	public const int POINTING_ID_NONE = 100;
	public const int SHAPE_ID_TETRAHEDRON = 0;
	public const int SHAPE_ID_OCTAHEDRON = 1;
	public const int SHAPE_ID_CUBE = 2;
	public const int SHAPE_ID_COMPOUND = 3;
	public const int AW_ID_EASY = 0;
	public const int AW_ID_NORMAL = 1;
	public const int AW_ID_HARD = 2;

	public TargetInstantiation ti;
	public Material currentTargetMaterial;
	public Material targetMaterial;
	public bool isCycleCompleted;
	public SceneController sceneController;

	private static GameController gameController;
	private SessionController s;
	private GameObject currentTarget;
	private GameObject previousTarget;
	private int currentTargetIndex;
	private int orderIndex;
	private int connectedPointingID;
	private List<string[]> rowData;
	private SubtaskData subtaskData; //data of current subtask in play
	private CycleData cycleData; //data of series from subtasks (whole cycle data)


	void Start(){
		isCycleCompleted = false;
		rowData = new List<string[]> ();
		cycleData = new CycleData ();
		previousTarget = null;
		currentTargetIndex = 0;
		orderIndex = 0;
		s = SessionController.session;

		AddPreliminarySubtaskData ();
	}

	public static GameController Instance () {
		if (!gameController) {
			gameController = FindObjectOfType (typeof(GameController)) as GameController;
			if (!gameController)
				Debug.Log ("There needs to be one active GameController script on a GameObject in your scene!");
		}
		return gameController;
	}

	public void HighlightNextTarget (GameObject player, bool isHit, DateTime timestamp){
		if (previousTarget != null) { //reset material to normal target (not highlighted)
			previousTarget.GetComponent<Renderer> ().material = targetMaterial;
			previousTarget.tag = "Target";
		}
		if (orderIndex == 1) { //set data of first subtask from first target
			subtaskData = SetSubtaskDataAtStart (player, currentTarget, timestamp);
		} else if (orderIndex > 1 && orderIndex != ti.targetOrder.Length) { //set end than start of subtask data for each step (target)
			SetSubtaskDataAtEnd (subtaskData, player, currentTarget, timestamp, isHit);
			subtaskData = SetSubtaskDataAtStart (player, currentTarget, timestamp);
		}
		if (orderIndex == ti.targetOrder.Length){ //if test is completed set end subtask data for last subtask
			isCycleCompleted = true;
			SetSubtaskDataAtEnd (subtaskData, player, currentTarget, timestamp, isHit);
			AddPreliminaryCycleData ();
			AddCycleData (cycleData);
			SaveData ();

			Cursor.visible = true;
			sceneController.RestartPanel ();
		} else { //highlight next
			currentTargetIndex = ti.targetOrder [orderIndex];
			currentTarget = ti.targets [currentTargetIndex];

			currentTarget.GetComponent<Renderer> ().material = currentTargetMaterial;
			currentTarget.tag = "CurrentTarget";

			orderIndex++;
			previousTarget = currentTarget;
		}
	}

	public float GetSphereDiameter(GameObject sphere){
		return sphere.GetComponent<SphereCollider> ().radius * 
			(Mathf.Max(sphere.transform.lossyScale.x, sphere.transform.lossyScale.y, sphere.transform.lossyScale.z) * 2);
	}

	private Vector3 ProjectPointOnLine(Ray ray, Vector3 pointToProject){ //ray is the line to project onto
		Vector3 V1 = pointToProject - ray.origin;
		Vector3 V2 = Vector3.Project(V1, ray.direction);
		return ray.origin + V2;
	}

	private string GetPointingID(){
		switch (s.pointingID) {
		case POINTING_ID_MOUSE:
			return "mouse";
		case POINTING_ID_GAME:
			return "game_ctrl";
		case POINTING_ID_LEAP:
			return "leap";
		default:
			return "unknown";
		}
	}

	private string GetShapeID(){
		switch (s.shapeID) {
		case SHAPE_ID_TETRAHEDRON:
			return "tetra";
		case SHAPE_ID_OCTAHEDRON:
			return "octa";
		case SHAPE_ID_CUBE:
			return "cube";
		case SHAPE_ID_COMPOUND:
			return "compound";
		default:
			return "unknown";
		}
	}

	private string ToStringWithComma(float floatNum){
		return string.Format (System.Globalization.CultureInfo.GetCultureInfo ("de-DE"), "{0:0.000}", floatNum);
	}

	private string GetHitString(bool isHit){
		if (isHit)
			return "Hit";
		else
			return "Miss";
	}

	private SubtaskData SetSubtaskDataAtStart (GameObject player, GameObject target, DateTime timestamp){ //set starting data of subtask in subtask object
		Debug.Log ("Adding subtask data at START!");
		SubtaskData subtask = new SubtaskData (GetSphereDiameter (player), target.transform.position, player.transform.position, timestamp);

		return subtask;
	}

	private void SetSubtaskDataAtEnd(SubtaskData subtask, GameObject player, GameObject target, DateTime timestamp, bool isHit){
		Debug.Log ("Adding subtask data at END!");
		subtask.TargetDiameter = GetSphereDiameter (target);
		subtask.To = target.transform.position;
		subtask.RealTo = player.transform.position;
		subtask.IsHit = isHit;
		subtask.EndTimestamp = timestamp;

		Ray tempRay = new Ray(subtask.RealFrom, subtask.To-subtask.RealFrom);

		subtask.TTo = ProjectPointOnLine (tempRay, subtask.RealTo);

		AddSubtaskDataToRow(subtask);
	}

	private void AddPreliminarySubtaskData(){ //add preliminary data for headings
		Debug.Log ("Adding preliminary subtask data");
		string[] rowDataTemp = new string[16]{"userID",
			"pointingID",
			"shapeID",
			"Cursor diameter",
			"Target diameter",
			"FROM", 	//xyz coordinates of starting sphere
			"TO", 		//xyz coordinates of target sphere
			"REAL_FROM", //xyz coordinates of cursor at start of subtask
			"REAL_TO", //xyz coordinates of cursor at end of subtask
			"t_TO", //xyz coordinates of the projected REAL_TO onto task axis
			"Hit/Miss",
			"Start timestamp",
			"End timestamp",
			"Duration (ms)", //how long it took to complete subtask
			"dx", //distance between projection of REAL_TO onto task axis and TO
			"Ae" //effective value of A
		};

		rowData.Add (rowDataTemp);
	}

	private void AddSubtaskDataToRow(SubtaskData subtask){ //add data to rowData
		string[] rowDataTemp = new string[16];

		rowDataTemp [0] = s.userID;
		rowDataTemp [1] = GetPointingID ();
		rowDataTemp [2] = GetShapeID ();
		rowDataTemp [3] = ToStringWithComma(subtask.CursorDiameter);
		rowDataTemp [4] = ToStringWithComma(subtask.TargetDiameter);
		rowDataTemp [5] = subtask.From.ToString ("F3");
		rowDataTemp [6] = subtask.To.ToString ("F3");
		rowDataTemp [7] = subtask.RealFrom.ToString ("F3");
		rowDataTemp [8] = subtask.RealTo.ToString ("F3");
		rowDataTemp [9] = subtask.TTo.ToString ("F3");
		rowDataTemp [10] = GetHitString(subtask.IsHit);
		rowDataTemp [11] = String.Format("{0:dd.MM.yyyy HH:mm:ss}", subtask.StartTimestamp); //add .fff at and of format for miliseconds
		rowDataTemp [12] = String.Format("{0:dd.MM.yyyy HH:mm:ss}", subtask.EndTimestamp);
		rowDataTemp [13] = ToStringWithComma(subtask.Duration);
		rowDataTemp [14] = ToStringWithComma (subtask.Dx);
		rowDataTemp [15] = ToStringWithComma (subtask.Ae);

		rowData.Add (rowDataTemp);

		cycleData.Subtasks.Add (subtask);
		subtaskData = null;
	}

	private void AddPreliminaryCycleData(){
		Debug.Log ("Adding preliminary cycle data");
		string[] rowDataTemp = new string[8] {"Start of task",
			"A", 
			"IDn",
			"Ae (mean)",
			"We",
			"IDe",
			"MT (ms)",
			"Throughput (bits/s)"
		};

		rowData.Add (rowDataTemp);
	}

	private void AddCycleData(CycleData cycle){
		Debug.Log ("Adding cycle data");
		string[] rowDataTemp = new string[8];

		rowDataTemp [0] = String.Format ("{0:dd.MM.yyyy HH:mm:ss}", cycle.Subtasks[0].StartTimestamp);
		rowDataTemp [1] = ToStringWithComma (cycle.A);
		rowDataTemp [2] = ToStringWithComma (cycle.Idn);
		rowDataTemp [3] = ToStringWithComma (cycle.AeMean);
		rowDataTemp [4] = ToStringWithComma (cycle.We);
		rowDataTemp [5] = ToStringWithComma (cycle.Ide);
		rowDataTemp [6] = ToStringWithComma (cycle.Mt);
		rowDataTemp [7] = ToStringWithComma (cycle.Throughput);

		rowData.Add (rowDataTemp);
	}


	private void SaveData(){
		Debug.Log ("Saving!");
		string[][] output = new string[rowData.Count][];
		string fittsDataPath;

		fittsDataPath = Application.dataPath + "/Fitts3D_Data";

		for(int i = 0; i < output.Length; i++){
			output[i] = rowData[i];
		}

		int length = output.GetLength(0);
		string delimiter = ";";

		StringBuilder sb = new StringBuilder();

		for (int index = 0; index < length; index++)
			sb.AppendLine(string.Join(delimiter, output[index]));

		if (!Directory.Exists (fittsDataPath)) {
			Directory.CreateDirectory (fittsDataPath);
		} 

		string filePath = fittsDataPath + "/" + s.userID + "_" + GetPointingID() + ".csv";

		StreamWriter outStream = (File.Exists(filePath)) ? File.AppendText(filePath) : File.CreateText(filePath);

		outStream.WriteLine(sb);
		outStream.Close();
	}
}