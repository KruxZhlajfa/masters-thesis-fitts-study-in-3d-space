﻿using UnityEngine;
using System;
using System.Collections;

public class SubtaskData{

	private float cursorDiameter; //player sphere diameter
	private float targetDiameter; //diameter of the target sphere in the subtask
	private Vector3 from; //the set FROM point of the subtask
	private Vector3 to; //the set TO point of the subtask
	private Vector3 realFrom; //real point where user has clicked at the start of the subtask
	private Vector3 realTo; //real point where user has clicked at the end of the subtask
	private Vector3 tTo; //projected REAL_TO onto task axis (task axis is REAL_FROM -> TO)
	private bool isHit; //did the user hit the target of the subtask
	private DateTime startTimestamp; //when did the subtask start
	private DateTime endTimestamp; //when did the subtask end


	public SubtaskData(float cursorDiameter, Vector3 from, Vector3 realFrom, DateTime startTimestamp){
		this.cursorDiameter = cursorDiameter;
		this.from = from;
		this.realFrom = realFrom;
		this.startTimestamp = startTimestamp;
	}

	public float CursorDiameter{ 
		get { return cursorDiameter; } 
		set { cursorDiameter = value; } 
	}

	public float TargetDiameter{ 
		get { return targetDiameter; } 
		set { targetDiameter = value; } 
	}

	public Vector3 From{
		get { return from; }
		set { from = value; }
	}

	public float FromX{ 
		get { return from.x; } 
		set { from.x = value; } 
	}

	public float FromY{ 
		get { return from.y; } 
		set { from.y = value; } 
	}

	public float FromZ{ 
		get { return from.z; } 
		set { from.z = value; } 
	}

	public Vector3 To{
		get { return to; }
		set { to = value; }
	}

	public float ToX{ 
		get { return to.x; } 
		set { to.x = value; } 
	}

	public float ToY{ 
		get { return to.y; } 
		set { to.y = value; } 
	}

	public float ToZ{ 
		get { return to.z; } 
		set { to.z = value; } 
	}

	public Vector3 RealFrom{
		get { return realFrom; }
		set { realFrom = value; }
	}

	public float RealFromX{ 
		get { return realFrom.x; } 
		set { realFrom.x = value; } 
	}

	public float RealFromY{ 
		get { return realFrom.y; } 
		set { realFrom.y = value; } 
	}

	public float RealFromZ{ 
		get { return realFrom.z; } 
		set { realFrom.z = value; } 
	}

	public Vector3 RealTo{
		get { return realTo; }
		set { realTo = value; }
	}

	public float RealToX{ 
		get { return realTo.x; } 
		set { realTo.x = value; } 
	}

	public float RealToY{ 
		get { return realTo.y; } 
		set { realTo.y = value; } 
	}

	public float RealToZ{ 
		get { return realTo.z; } 
		set { realTo.z = value; } 
	}

	public Vector3 TTo{
		get { return tTo; } 
		set { tTo = value; } 
	}

	public bool IsHit{ 
		get { return isHit; } 
		set { isHit = value; } 
	}

	public DateTime StartTimestamp{ 
		get { return startTimestamp; } 
		set { startTimestamp = value; } 
	}

	public DateTime EndTimestamp{ 
		get { return endTimestamp; } 
		set { endTimestamp = value; } 
	}

	public float Duration{ 
		get {
			return (float) (this.endTimestamp - this.startTimestamp).TotalMilliseconds; 
		}
	}

	public float Dx{
		get {
			if (Vector3.Distance(this.realFrom, this.tTo) >= Vector3.Distance(this.realFrom, this.to)){
				return Vector3.Distance (this.tTo, this.to);
			} else{
				return -(Vector3.Distance (this.tTo, this.to));
			}
		}
	}

	public float Ae{
		get {
			return Vector3.Distance (this.realFrom, this.tTo);
		}
	}
}