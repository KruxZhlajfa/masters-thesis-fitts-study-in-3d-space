﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.Collections;

public class SceneController : MonoBehaviour {

	private PopupPanel popupPanel;
	private UnityAction positiveAction;
	private UnityAction negativeAction;
	private SessionController s;

	void Start (){
		popupPanel = PopupPanel.Instance ();
		s = SessionController.session;
	}

	public void ExitPanel(){
		positiveAction = new UnityAction (ExitApplication);
		negativeAction = new UnityAction (ClosePopup);

		popupPanel.popup ("Are you sure you want to exit application?", null, true, "Yes", "No", false, null, null, positiveAction, negativeAction);
	}

	public void ErrorPanel(string errorMsg){
		popupPanel.popup (null, errorMsg, false, null, null, false, null, null, null, null);
	}

	public void RestartPanel(){
		positiveAction = new UnityAction (delegate {
			LoadScene ("StartMenu");
		});
		negativeAction = new UnityAction (ExitApplication);
		string first, second;
		if (s.pointingID == GameController.POINTING_ID_GAME) {
			first = "X: Yes";
			second = "O: Exit";
		} else {
			first = "Y: Yes";
			second = "E: Exit";
		}

		popupPanel.popup ("Well done! Do you want to go to the main menu?", null, true, "Yes", "No, exit app", true, first, second, positiveAction, negativeAction);
	}

	public void ClosePopup(){
		popupPanel.ClosePanel ();
	}

	public void LoadScene(string name){
		SceneManager.LoadScene (name);
	}

	void ExitApplication(){
		Application.Quit ();
	}

	public void userIDChanged(string newUserID){
		s.userID = newUserID;
	}

	public void pointingIDChanged(int newPointingID){
		s.pointingID = newPointingID;
	}

	public void shapeIDChanged(int newShapeID){
		s.shapeID = newShapeID;
	}

	public void AWChanged(int newAW){
		s.AW = newAW;
	}
}
