﻿using UnityEngine;
using System.Collections;

public class SessionController : MonoBehaviour {

	public static SessionController session;

	public string userID;
	public int pointingID;
	public int shapeID;
	public int AW;

	void Awake(){
		if (session == null) {
			session = this;
		} else if (session != this){
			Destroy (gameObject);
		}

		DontDestroyOnLoad (gameObject);
	}
}
